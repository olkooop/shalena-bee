class Monster:
    '''
    Class that represents Monster character
    '''
    def __init__(self, name, hp, damage, speed, exp):
        self.name = name
        self.hp = hp
        self.damage = damage
        self.speed = speed
        self.exp = exp
        self.status = "alive"

    def do_damage(self):
        '''
        Method for doing damage to main hero
        '''
        enemy_damage = self.damage * self.speed
        print(
            f"{self.name.title()} attacked hero and "
            f"dealed {enemy_damage} damage."
        )
        return enemy_damage

    def take_damage(self, hero_damage):
        '''
        Method for taking damage from main hero
        '''
        self.hp -= hero_damage
        print(
            f"{self.name.title()} received {hero_damage} damage."
            f"He got {self.hp} hp left"
        )
        if self.hp <= 0:
            print(f"{self.name.title()} is Dead!")
            self.status = "dead"
            return self.exp

        return self.status

