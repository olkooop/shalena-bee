class Hero:
    '''
    Class that represents Hero character
    '''
    def __init__(self, name, hp, damage, speed, exp, level):
        self.name = name
        self.hp = hp
        self.damage = damage
        self.speed = speed
        self.exp = exp
        self.level = level
        self.status = "alive"

    def _get_level(self):
        '''
        Helper method for getting next level
        '''
        self.level += 1
        self.speed += 1
        self.hp += 10
        print(
            f"Congratulation! {self.name.title()} received {self.level} level!"
        )
        return self.level

    def _heal_hp(self):
        '''
        Helper method which heal hero hp
        '''
        holy_restoration = self.level * 10 + 100 / 2.5
        self.hp += holy_restoration
        print(
            f'{self.name.title()} used skill "Holy restoration" '
            f'and heal himself for {holy_restoration} hp.'
        )

    def do_damage(self, enemy_name):
        '''
        Method for doing damage to enemy
        '''
        hero_damage = self.damage * self.speed
        print(
            f"Hero attacked {enemy_name.title()} with his sword and "
            f"dealed {hero_damage} damage."
        )
        return hero_damage

    def take_damage(self, enemy_damage):
        '''
        Method for taking damage from enemy
        '''
        self.hp -= enemy_damage
        print(
            f"{self.name.title()} received {enemy_damage} damage."
            f"He got {self.hp} hp left"
        )
        if self.hp <= 0:
            print(f"{self.name.title()} is Dead!")
            self.status = "dead"
            return self.status
        elif self.hp <= self.level * 100 / 10:
            self._heal_hp()
        return self.status

    def get_exp(self, enemy_exp):
        '''
        Method for getting experience from enemies death
        '''
        self.exp += enemy_exp
        if self.exp >= 100 * self.level:
            self._get_level()
        print(f"{self.name.title()} get {enemy_exp} experience points.")
        return self.exp

    def show_stat(self):
        '''
        Method for showing stat
        '''
        print(f"{self.name.title()}'s health points is {self.hp}.")
        print(f"{self.name.title()}'s level is {self.level}.")
